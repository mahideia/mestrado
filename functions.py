# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 19:35:59 2016

@author: marina
"""


import numpy as np
import datetime
#%% calcula dia do ano 
def day_of_year(date):
    fmt = '%Y %m %d'
    dt = datetime.datetime.strptime(date, fmt)
    tt = dt.timetuple()
    return tt.tm_yday
    

#%% calcula hr(decimal) umt

def time_decimal(time,delimiter):
    (h, m, s) = time.split(delimiter)
    return int(h)+ int(m)/60 + int(s)/3600 

#%% calcula qual o tempo/dia seguinte (no caso qual o tempo da coluna seguinte) somando o passo
def next_datetime(date,time,step):
    #pega data
    (yy,mm,dd) = date.split(' ')
    #pega hora
    (h, m, s) = time.split(' ')
    #combina num datetime s´o
    d = datetime.datetime.combine(datetime.date(int(yy),int(mm), int(dd)), datetime.time(int(h),int(m), int(s)))
    #soma o step, que vai ser dado em minutos
    step = step*60
    d = d + datetime.timedelta(seconds=step)
    #devolve lista simples onde  primeiro campo é a data e segundo campo é o tempo
    a = d.strftime("%Y %m %d %H:%M:%S")
    b = d.strftime("%H %M %S")
    return [a,b]

    

#%%

TESTE = ''

def teste():
    return TESTE

def Q(wl):
    if wl == 0.5: 
        return 2.0986874703
    elif wl == 0.66:
        return 2.1193181648    
    elif wl == 3.9:
        return 2.436791927
    elif wl == 0.55:
        return 2.105263387 
    else:
        return 2.00

def TCLOUD(cod,wl):
    return cod * Q(0.55)/Q(wl)

#escreve usrcld.dat lwp(i),re(i),frozen water path,reff ice(i),cldfrac  
def lwp(param):
    #calcula lwp
    lwp = param['cod']*param['reff']/Q(param['wavelength'])*4/3
    return lwp

def lwp_sbd(cod,reff,wl):
    #calcula lwp
    lwp = cod*reff/Q_water(wl,reff)*4/3
    return lwp    
    
def cod_sbd(lwp,reff,wl):
    cod = lwp*Q_water(wl,reff)/reff*3/4
    return cod
    
def strL(coisa):
    x = str(coisa).replace(",","")
    x = x.replace("[","")
    x = x.replace("]","")
    x = x.replace("'","")
    x = x.replace('"','')
    return x

def strC(coisa):
    x = str(coisa).replace("[","")  
    x = x.replace("]","")
    return x


#%%funcoes necessárias para leitura do output do sbdart
def leLinha(caminho,arquivo,linha):
    f= open(caminho + arquivo, 'r')
    for i in range(linha): l = f.readline()
    return np.fromstring(l,sep='  ')


    
def readSBDIrradiance(caminho,arquivo,filterfunction):
        #lẽ primeira linha, onde tem dados de irrad 
        #estrutura = ['TOPDN','TOPUP','TOPDIR','BOTDN','BOTUP','BOTDIR']
    if filterfunction == 'yes':
        dados = np.transpose(leLinha(caminho,arquivo,1))
        lido = dados[3:]
    else:
        dados = np.transpose(leLinha(caminho,arquivo,4))
        lido = dados[2:]
    return lido
        
def readSBDRadiance(caminho, arquivo,filterfunction):
         #lê linhas com dados de radiancia. 
    if filterfunction == 'yes':
        rads = np.loadtxt(caminho+arquivo,skiprows=4)
    else:
        rads = np.loadtxt(caminho+arquivo,skiprows=7) #REVER ESQUEMA LINHA/COLUNA#REVER ESQUEMA LINHA/COLUNA        
    return rads
        
#def readLRTIrradiance(caminho,arquivo):
#        #lẽ primeira linha, onde tem dados de irrad 
#        #estrutura = ['TOPDN','TOPUP','TOPDIR','BOTDN','BOTUP','BOTDIR']
#    dados = np.transpose(leLinha(caminho,arquivo,1))
#    lido = dados[1:]
#    return lido
#    
#def readLRTRadiance(caminho, arquivo):
#         #lê linhas com dados de radiancia. 
#    rads = np.loadtxt(caminho+arquivo,skiprows=2) #REVER ESQUEMA LINHA/COLUNA
#    rads = np.transpose(np.transpose(rads)[2:])
#    return rads
      
def readLRTIrradiance(arquivo):
    #lẽ primeira linha, onde tem dados de irrad 
    #estrutura = ['TOPDN','TOPUP','TOPDIR','BOTDN','BOTUP','BOTDIR']
    #arquivo.seek(0)
    dados = np.transpose(leLinha2(arquivo,1))
    lido = dados[1:]
    return lido
    
def readLRTRadiance(arquivo):
     #lê linhas com dados de radiancia. 
    #arquivo.seek(0)
    rads = np.loadtxt(arquivo,skiprows=2) #REVER ESQUEMA LINHA/COLUNA
    rads = np.transpose(np.transpose(rads)[2:])
    return rads


    
def leLinha2(arquivo,linha):
    f= open(arquivo, 'r')
    for i in range(linha): l = f.readline()
    return np.fromstring(l,sep='  ')    
    

  
#%% funções para leitura de irradiâncias no sbdart e no libradtran (iout 1, 10 ou umu=0)
def readIrradiance(model, caminho, arquivo):
    if model == 'sbdart':
        dados = np.transpose(leLinha(caminho,arquivo,4))
        lido = dados[2:]
        return lido
    if model == 'libRadtran':
        dados = np.transpose(leLinha(caminho,arquivo,1))
        lido = dados[1:]
        return lido
        
        
#%% lê time lê 
def to_datetime(date,time):
    #há uma pequena variação nos segundos dos dados mas eu vou fingir que é tudo perfeito
    #pega data
    (yy,mm,dd) = date.split(' ')
    #pega hora
    (h, m, s) = time.split(' ')
    #combina num datetime s´o
    d = datetime.datetime.combine(datetime.date(int(yy),int(mm), int(dd)), datetime.time(int(h),int(m),int(s)))
    #devolve datetime
    return d
  #%%  
def linha_saida(param):
    #pega o que é pra sair numa linha no output devido
    keys = param['output'][param['model']]
    linha = '' 
    
    for key in keys:
        linha = linha + strL(param[key])  + ' '
        
    linha = linha + ' \n'
    return linha
        
def make_head(param,modelo): 
    #pega as chaves com base no que está em POSSIBLE_VARIABLES
    linha = '' 
    keys = param['output'][modelo]
    
    for key in keys:
        linha = linha + strL(key)  + ' '
        
    linha = linha + ' \n'
    return linha
    
    #%%
def reflectance(L,I,sza):
    mu = 1 #np.cos(np.radians(sza))
    rho = (np.pi * L)/(mu*I)
    return rho
    
    #%%
def nome_arquivo(param):
    if param['model'] == 'sbdart':
        arquivo = 'sbd_'
        arquivo = arquivo + str(param['wavelength']) + '_cod' + str(param['cod']) + '_reff' + str(param['reff'])
        arquivo = arquivo + '_alb' + str(param['albedo']) + '_sza' + str(param['sza']) + '_nstr' + str(param['nstr']) + '_'
        arquivo = arquivo + str(param['tipo_sbdart']) + '_' + str(param['solver_sbdart'])
        arquivo = arquivo + '.out'
    if param['model'] == 'libRadtran':
        arquivo = 'lrt_'
        arquivo = arquivo + str(param['wavelength']) + '_cod' + str(param['cod']) + '_reff' + str(param['reff'])
        arquivo = arquivo + '_alb' + str(param['albedo']) + '_sza' + str(param['sza']) + '_nstr' + str(param['nstr']) + '_'
        if param['solver_libRadtran']=='none': 
            arquivo = arquivo + str(param['tipo_libRadtran'])
        else:
            arquivo = arquivo + str(param['tipo_libRadtran']) + '_' + str(param['solver_libRadtran'])  
        arquivo = arquivo + '.out'
    return arquivo
    
#%%#%% LRT - leitura de arquivo

def read_store_output_libradtran(caminho_leitura, arquivo_leitura,param,caminho_saida,arquivo_saida):
    if param['output_user'] == 'yes':
        #tudo vai vir numa linha só e o uu vai vir uu(umu(0),phi(0)) ... uu(umu(0),phi(m)) ... uu(umu(n),phi(0)) ...uu(umu(n),phi(m))
        #wl edir edn eup eglo uu
        #le a primeira e unica linha
        lrt_out = open(caminho_saida + arquivo_saida, "a")
        saida = readLRTIrradiance(caminho_leitura+arquivo_leitura)
        param['edir'] = saida[0]
        param['edn'] = saida[1]
        param['eup'] = saida[2]
        param['eglo'] = saida[3]
                   
        count = 3
        for u in param['uz']:
            for phi in param['phis']:
                count = count +1
                param['uzen'] = u
                param['phi'] = phi
                param['radiance'] = saida[count]
                param['reflectance']=reflectance(param['radiance'], param['edir'], param['sza']) 
                lrt_out.write(linha_saida(param))
                print 'salvando linha lrt '
    else:
                
        if param['solver_libRadtran']!='twostr':
            if param['uzens']!=0:
                uz=[]                        
                for uzen in param['uzens']:
                    uz.append(uzen)
                uz.reverse()
        
            lrt_out = open(caminho_saida + arquivo_saida, "a")
            #primeiro lê as irradiâncias
            param['model']='libradtran'
            irrad = readLRTIrradiance(caminho_leitura+arquivo_leitura)
            param['edir']=irrad[0]
            param['edn']=irrad[1]
            param['eup']=irrad[2]
            param['uavgdir']=irrad[3]
            param['uavgdn']=irrad[4]
            param['uavgup']=irrad[5]
            
            #lê output de radiância: devolve um array com
            if len(param['phis'])!=0:
                rads = readLRTRadiance(caminho_leitura+arquivo_leitura)
                #guarda cada radiancia                                           
                for i in range(len(param['uzens'])):
                    for j in range(len(param['phis'])):
                        
                        param['uzen'] = uz[i]
                        param['phi'] = param['phis'][j]
                        param['radiance']=rads[i,j]                    
                
                        param['reflectance']=reflectance(param['radiance'],param['edir'],param['sza'])
                        lrt_out.write(linha_saida(param))
                        print 'salvando linha lrt '
            else:
                lrt_out.write(linha_saida(param))
        else:
            lrt_out = open(caminho_saida + arquivo_saida, "a")
            irrad = readLRTIrradiance(caminho_leitura,arquivo_leitura)
            param['edir']=irrad[0]
            param['edn']=irrad[1]
            param['eup']=irrad[2]
            lrt_out.write(linha_saida(param))
    lrt_out.close()
    
#%% SBDART - leitura de arquivo

def read_store_output_sbdart(caminho_leitura, arquivo_leitura, param, caminho_saida, arquivo_saida):
    #abre arquivo onde será gravado
    sbd_out = open(caminho_saida+arquivo_saida,"a")    
    #lê irradiâncias    
    irrad = readSBDIrradiance(caminho_leitura,arquivo_leitura,param['filterfunction'])
    param['topdn']=irrad[0]
    param['topup']=irrad[1]
    param['topdir']=irrad[2]
    param['botdn']=irrad[3]
    param['botup']=irrad[4]
    param['botdir']=irrad[5]
    
    #lê output de radiância: devolve um array com dimensões 
    rads = readSBDRadiance(caminho_leitura,arquivo_leitura,param['filterfunction'])
    #print "RADS: " + str(rads) + "\n\n\n"
    #guarda cada radianciaset_default(param)
    if len(param['phis'])!=0:
        if len(param['uzens']) ==1:
            for j in range(len(param['phis'])):
                param['uzen'] = param['uzens']
                param['phi'] = param['phis'][j]
                param['radiance']=rads[j]
                param['reflectance']=reflectance(param['radiance'],param['topdn'],param['sza'])                                    
                sbd_out.write(linha_saida(param))
                print 'salva linha sbdart'
        else:
            for i in range(len(param['uzens'])):
                for j in range(len(param['phis'])):
                                    
                    param['uzen'] = param['uzens'][i]
                    param['phi'] = param['phis'][j]
                    param['radiance']=rads[i,j]
                    param['reflectance']=reflectance(param['radiance'],param['topdn'],param['sza'])
                    sbd_out.write(linha_saida(param))
                    print 'salvando linha sbd '
    else:
        sbd_out.write(linha_saida(param))
        
    sbd_out.close()

#########################################
# Funções para as nuvens
#%% 
def cod_ice(cod,wl,reff):
    #faz aquela coisa de calcular considerando Q(wl) pro gelo etc
    codice = cod * Q_ice(550,reff)/Q_ice(wl,reff)
    return round(codice,3)#retorna str já arredondado com duas casas decimais
#%%
def cod_water(cod,wl,reff):
    #faz aquela coisa de calcular considerando Q(wl) pra agua liq etc

    codagua = cod * Q_water(550,reff)/Q_water(wl,reff)
 
    return round(codagua,3)#retorna str já arredondado com duas casas decimais


#%%
def Q_water(wl, reff):
    #return 2.105263387 
    
    if wl == 500: 
        return 2.08859885
    elif wl == 660:
        return 2.10740292
        
    elif wl == 3900:
        return 2.37713826
    elif wl == 550:
        return 2.105263387 
    elif wl == 3750:
        if reff==3:
            return 3.06819
        if reff==4:
            return 3.18417
        if reff==6:
            return 2.70609
        if reff==8:
            return 2.40807
        if reff==12:
            return 2.31392
        if reff==16:
            return 2.24655
        if reff==24:
            return 2.19262
        if reff==32:
            return 2.15125
        else:
            return 2.2
    elif wl == 640:
        if reff==3:
            return 2.24296
        if reff==4:
            return 2.18977
        if reff==6:
            return 2.14867
        if reff==8:
            return 2.11703
        if reff==12:
            return 2.09245
        if reff==16:
            return 2.07326
        if reff==24:
            return 2.05805
        if reff==32: 
            return 2.04600
        else:
            return 2.00
    else:
        return 2.00
            
#%%
def Q_ice(wl):
    if wl == 500: 
        return 2.03096
    elif wl == 660:
        return 2.03741
    elif wl == 3900:
        return 2.12158
    elif wl == 550:
        return 2.105263387 
    else:
        return 2.00




#%%
def Q_ext(wl,reff, tipo):
    if tipo == 'water':
        A = 2.24486 #coloquei isso aqui pq tava dando erro quando rodava
        B = -0.823003
        C = 2.02363
        if wl <2000:
            A = 0.494135
            B=-0.708335
            C=2.0029
        else:
            if 2 <= reff < 8:
                A = 5.29378
                B = -0.530608
                C = 0.677346
            if 8<=reff<160:
                A = 2.24486
                B = -0.823003
                C = 2.02363
    if tipo == 'ice':        
        if wl<2000:
            A = 0.4985
            B = -0.707445
            C = 2.00197
        else:
            if reff < 8:
                A = 9.52509
                B = -1.52575
                C = 2.01339
            elif 8 <= reff <=190:
                A = 1.89139
                B = -0.746449
                C = 2.01134
    return A*reff**B+C

#%%
def salvarResultado(arquivo, dicresultado, header): #vai salvar sem o header a pessoa que se vire
      
    # Abre arquivo onde será gravado
    
    saida = open(arquivo, "a");
    #saida.write(' '.join(header) + '\n')

    saida.write(' '.join(str(dicresultado[key]).strip("[],'\"") for key in header) + '\n')
    saida.flush()            
    

#%%
