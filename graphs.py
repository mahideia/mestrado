# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 16:20:31 2016

@author: marina
"""

#%%imports necessários
import functions as f
import numpy as np
import matplotlib.pyplot as pl

#import make_data as m_data
import copy
#%%caminho para gravar a figura
pathGraph = None


#%% COISAS A IMPLEMENTAR
"""
uma propriedade style, que indica se é traço ou traço e ponto ou coisas assim
um array colors, que indica a cor a ser associada com cada um dos itens nas séries.

"""

#%%
class Graphs(): 
    #características do gráfico    
    title = None
    labelx = None
    labely = None
    figura = None
    #dados eixos
    x_axis = None #nome, do header, do que deve ir no eixo x
    y_axis = None #nome, do header, do que deve ir no eixo x
    xlim = None #TO DO inserir algo sobre limitar num intervalo só o negócio
    ylim = None # TO DO
    #ddados brutos
    data_header = None
    data = None
    condicoes = None
    nome_series = None #o que tá no header que identifica qual a série
    valores_series = None
    alias_series = None
    cores_series = None
    pontos = None
    
    ordem = None
    def plotGraph(self):
        #validações 
        if pathGraph is None:
            raise Exception('informar caminho para salvar figura')
        if (self.data is None) or (self.data_header is None):
            raise Exception('carregar dados e cabeçalho!')
        if self.nome_series is None:
            filtrados = self.filtra_dados(self.data,self.condicoes,self.data_header)
            filtrados = np.transpose(filtrados)
            x = filtrados[self.data_header.index(self.x_axis)]
            y = filtrados[self.data_header.index(self.y_axis)]
            if self.ordem == 'y':
                [x,y]=self.ordem_dados(x,y)
            pl.plot(x,y,'-o')
        else:
            if self.alias_series == None:
                self.alias_series = self.valores_series
            for valor in self.valores_series:
                self.condicoes[self.nome_series] = valor
                filtrados = self.filtra_dados(self.data,self.condicoes,self.data_header)
                filtrados = np.transpose(filtrados)
                x = filtrados[self.data_header.index(self.x_axis)]
                y = filtrados[self.data_header.index(self.y_axis)]
                if self.ordem == 'y':
                    [x,y]=self.ordem_dados(x,y)
                if self.pontos == 'y':
                     pl.plot(x,y,'-o',label=str(self.alias_series[self.valores_series.index(valor)]),color=str(self.cores_series[self.valores_series.index(valor)]))
                else:
                     pl.plot(x,y,'-',label=str(self.alias_series[self.valores_series.index(valor)]),color=str(self.cores_series[self.valores_series.index(valor)]))
        pl.ylabel(self.labely)
        pl.xlabel(self.labelx)
        if self.ylim is not None:
            pl.ylim(self.ylim[0],self.ylim[1])
        if self.xlim is not None:
            pl.xlim(self.xlim[0],self.xlim[1])
        pl.legend(loc="best", shadow=False, fontsize='small')
        pl.title(self.title)
        pl.savefig(pathGraph + self.figura)
        pl.show()
        
    def ordem_dados(self,x,y):
        #colocar os dados em ordem pra plotar (posso comentar se for o caso)
        d=np.array([x,y])
        d = np.transpose(d)
        d.sort(axis=0)
        d = np.transpose(d)
        x =d[0]
        y=d[1]
        return [x,y]

        
    def filtra_dados(self,dados_brutos,condicoes,header):
        #talvez colocar aqui uma validação. se len dadosbrutos != de len header então fazer o transpose. (se tiver mesmo poucas linhas vai dar bosta)
        dados_brutos = np.transpose(dados_brutos)
        
        #pega  indices que atendenm as condições
        x = {}
        for c in condicoes:
            x[c] = dados_brutos[header.index(c)]
            x[c] = np.where(x[c]==condicoes[c])[0]
            
        #indices =  np.array(range(100000))  #resolver isso aqui!
        #for c in condicoes:
        indices = None
        for c in x:
            if indices is None:
                indices = x[c]
            else:
                indices = np.intersect1d(indices,x[c])
        
        dados_filtrados = np.transpose(dados_brutos)
        dados_filtrados = dados_filtrados[indices]
        #dados_filtrados = np.transpose(dados_filtrados)
        
        return dados_filtrados
    