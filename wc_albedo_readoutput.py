# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 10:15:57 2017

@author: marina
"""

#%% imports
import numpy as np
import functions as f
import matplotlib.pyplot as pl

#%%
def lerResultadoOutput(arquivo_leitura, arquivo_grava, param, header):
    resultado = {}
    for k in param:
        resultado[k] = param[k]
        
    saida = np.loadtxt(arquivo_leitura)
    resultado['edir'] = saida[1]
    resultado['edn'] = saida[2]
    resultado['eup'] = saida[3]
    resultado['eglo'] = saida[4]
       

    count = 4
    for u in param['uz']:
        resultado['uzen'] = u
        for phi in param['phis']:
            count = count +1
            resultado['phi'] = phi
            resultado['radiance'] = saida[count]
            resultado['reflectance']=f.reflectance(resultado['radiance'], resultado['edir'], 30) #o sza não importa no calculo
            f.salvarResultado(arquivo_grava, resultado, header)            
                        
#%% ler todas as saídas e gerar um txt com sza phi uzen edir edn eup eglo radiance reflectance

reff = 5

header = ['sza','phi','uzen','albedo','cod','edir','edn','eup','eglo','radiance','reflectance']
caminho_leitura = '/home/marina/Dropbox/mestrado/estudos_sim/wc_albedo/ch1/dados/out_reff'
arquivo_saida = '/home/marina/Dropbox/mestrado/estudos_sim/wc_albedo/ch1/dados/reff' + str(reff) + '.txt'
#arquivo_saida = '../estudos_sim/albedo_wc_out/reff5.txt'


albedos = [0.125,0.1,0.075,0.05,0.025,0]
cods =[10]
#cods = [0.1,1,10,25,50,75,100]
param = {}
param['phis'] = [30, 150]
param['uzens']=[5,10]
param['uz']=[10,5]
param['sza']=10
param['reffs'] = [5,10,15,20,30,40]


param['reff'] = reff

for cod in cods:
    param['cod']=cod
    for alb in albedos:
        param['albedo'] = alb
        nome_leitura = 'reff' + str(reff) +'_alb' + str(alb) + '_' + str(cod) + '.out'
        arquivo = caminho_leitura + str(reff) + '/' + nome_leitura
        lerResultadoOutput(arquivo, arquivo_saida,param,header)
        
        #lido = np.loadtxt('../estudos_sim/albedo_wc_out/' + nome_leitura)
        #lido = np.loadtxt('M:/Dropbox/mestrado/estudos_sim/albedo_wc_out/'+nome_leitura)
