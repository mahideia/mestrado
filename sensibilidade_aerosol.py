
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 25 21:27:30 2016

@author: marina
"""

"""
absorção molecular, uma comparação entre sbdart e libradtran 
"""

import sbdart
import libRadtran
import functions as f

sbdart.pathSBDART = "/home/marina/sbdart/"
libRadtran.pathLibRadtran = "/home/marina/Downloads/libRadtran-2.0/"
libRadtran.pathLibRadtran = "/home/marina/util/libRadtran-2.0/"
sbd = sbdart.SBDART()
lrt =  libRadtran.LibRadtran()

#%% itens fixos
#solar
sbd.nf = -1
lrt.source = 'solar solar_flux/kurudz_0.1nm.dat per_nm'
sbd.idatm = 1 #tropical
#altura
sbd.zout = [0,100]
lrt.zout = 100
sbd.height = 100
sbd.nothrm = 1
sbd.nstr = 24
#lrt.number_of_streams = 24 #para poder fazer sentido comparar com o sbdart
sbd.iout = 5

#geometria
sbd.sza = 60
lrt.sza = 60
sbd.phi = [30,150] #fazer tb [0,45]
lrt.phi = [30,150] 
sbd.uzen=[5,10] #fazer tb [0,15]
lrt.uzen = [5,10]

sbd.albcon = 0.1
lrt.albedo = 0.1


#%%
def set_default_mol(mod):
    if mod == sbd:
        sbd.xco2 = 360
        sbd.xch4 = 1.75
        sbd.uw = 4.5
        sbd.uo3 = 380
    if mod == lrt:
        lrt.mixing_ratio_CH4 = 1.75
        lrt.mixing_ratio_CO2 = 360
        lrt.mol_modify_H2O = '45 MM'
        lrt.mol_modify_O3 = '380 DU'
        lrt.xch4 = 1.5
        lrt.xco2 = 360
        lrt.h2o = 45
        lrt.o3 = 380
        
#%% default aerosol
def set_default_aerosol(mod):
    if mod == lrt:
        lrt.aerosol_default = 'yes'
        lrt.aerosol_modify_gg_set = 0.6
        lrt.gg = 0.6
        lrt.aerosol_modify_ssa_set = 0.9
        lrt.ssa = 0.9
        lrt.aerosol_modify_tau_set = 0.8
        lrt.aod = 0.8
        lrt.number_of_streams = 32
        lrt.nstr = 32
        
#%%
lrt.outputValues = ['sza','phi','uzen','abs_param','aod','ssa','gg','nstr','albedo','edn','eup','edir','radiance','reflectance']
lrt.rte_solver = 'disort'
lrt.output_process = 'per_nm'

absparam = ['lowtran','reptran fine']

for wl in [640, 3750, 3900, 550]:
    lrt.wavelength = wl
    for param in absparam:
        lrt.mol_abs_param = param
        lrt.abs_param = absparam.index(param)
        for atms in ['afglt']:
            lrt.textFile = 'txt_aer/aer' + str(wl) + '_'+ param +'_'+ atms + '_lrt.txt'
            for ssa in [0.6,0.7,0.8,0.9,1.0]:
                set_default_aerosol(lrt)
                lrt.ssa  = ssa
                lrt.aerosol_modify_ssa_set = str(ssa)
                lrt.run()
            for aod in [0,0.25,0.5,0.75,1.0,1.5,2.0,2.5,3.0]:
                set_default_aerosol(lrt)   
                lrt.aod = aod
                lrt.aerosol_modify_tau_set = str(aod)
                lrt.run()
            for gg in [0,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
                set_default_aerosol(lrt)
                lrt.gg = gg
                lrt.aerosol_modify_gg_set = str(gg)
                lrt.run()
            for nstr in [24,32,64]:
                set_default_aerosol(lrt)
                lrt.nstr = nstr
                lrt.number_of_streams = nstr
                lrt.run()   
                             
#%% nuvem de água
                
lrt.outputValues = ['sza','phi','uzen','abs_param','aod','ssa','gg','nstr','albedo','edn','eup','edir','radiance','reflectance']
lrt.rte_solver = 'disort'
lrt.output_process = 'per_nm'

absparam = ['lowtran','reptran fine']
cldparam = ['hu','mie interpolate']
lrt.typeCloud = 'water'
lrt.topCld = 2
lrt.baseCld = 1
lrt.reff = 18
lrt.lwc = 0.15

for wl in [640, 3750, 3900, 550]:
    lrt.wavelength = wl
    for wcprop in cldparam:
        lrt.wc_properties = wcprop
        for param in absparam:
            lrt.mol_abs_param = param
            lrt.abs_param = absparam.index(param)
            for atms in ['afglt']:
                lrt.textFile = 'txt_aer/agua' + str(wl) + '_'+ param +'_'+ atms +'_'+ wcprop + '_lrt.txt'
                for ssa in [0.6,0.7,0.8,0.9,1.0]:
                    set_default_aerosol(lrt)
                    lrt.ssa  = ssa
                    lrt.aerosol_modify_ssa_set = str(ssa)
                    lrt.run()
                for aod in [0,0.25,0.5,0.75,1.0,1.5,2.0,2.5,3.0]:
                    set_default_aerosol(lrt)   
                    lrt.aod = aod
                    lrt.aerosol_modify_tau_set = str(aod)
                    lrt.run()
                for gg in [0,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
                    set_default_aerosol(lrt)
                    lrt.gg = gg
                    lrt.aerosol_modify_gg_set = str(gg)
                    lrt.run()
                for nstr in [24,32,64]:
                    set_default_aerosol(lrt)
                    lrt.nstr = nstr
                    lrt.number_of_streams = nstr
                    lrt.run()            

#%% nuvem de gelo
lrt.outputValues = ['sza','phi','uzen','abs_param','aod','ssa','gg','nstr','albedo','edn','eup','edir','radiance','reflectance']
lrt.rte_solver = 'disort'
lrt.output_process = 'per_nm'

absparam = ['lowtran']#,'reptran fine']
cldparam = ['key']# fazer para: fu yang baum key baum_v36 hey
lrt.typeCloud = 'ice'
lrt.topCld = 9
lrt.baseCld = 8
lrt.reff = 32
lrt.iwc = 0.25
for wl in [640]:#, 3750, 3900, 550]:
    lrt.wavelength = wl
    for wcprop in cldparam:
        lrt.ic_properties = wcprop
        for param in absparam:
            lrt.mol_abs_param = param
            lrt.abs_param = absparam.index(param)
            for atms in ['afglt']:
                lrt.textFile = 'txt_aer/gelo_aer_' + str(wl) + '_'+ param +'_'+ atms +'_'+ wcprop + '_lrt.txt'
                for ssa in [0.6,0.7,0.8,0.9,1.0]:
                    set_default_aerosol(lrt)
                    lrt.ssa  = ssa
                    lrt.aerosol_modify_ssa_set = str(ssa)
                    lrt.run()
                for aod in [0,0.25,0.5,0.75,1.0,1.5,2.0,2.5,3.0]:
                    set_default_aerosol(lrt)   
                    lrt.aod = aod
                    lrt.aerosol_modify_tau_set = str(aod)
                    lrt.run()
                for gg in [0,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
                    set_default_aerosol(lrt)
                    lrt.gg = gg
                    lrt.aerosol_modify_gg_set = str(gg)
                    lrt.run()
                for nstr in [24,32,64]:
                    set_default_aerosol(lrt)
                    lrt.nstr = nstr
                    lrt.number_of_streams = nstr
                    lrt.run()  
                    
                    
#%% nuvem de água com alteração de o3
lrt.outputValues = ['sza','phi','uzen','abs_param','o3','aod','ssa','gg','nstr','albedo','edn','eup','edir','radiance','reflectance']
lrt.rte_solver = 'disort'
lrt.output_process = 'per_nm'
lrt.cloudFile = 'aer.dat'
absparam = ['lowtran','reptran fine']
cldparam = ['hu','mie interpolate']
lrt.typeCloud = 'water'
lrt.topCld = 2
lrt.baseCld = 1
lrt.reff = 18
lrt.lwc = 0.15

for wl in [640, 3750, 3900, 550]:
    lrt.wavelength = wl
    for wcprop in cldparam:
        lrt.wc_properties = wcprop
        for param in absparam:
            lrt.mol_abs_param = param
            lrt.abs_param = absparam.index(param)
            for atms in ['afglt']:
                lrt.textFile = 'txt_aer/agua_o3' + str(wl) + '_'+ param +'_'+ atms +'_'+ wcprop + '_lrt.txt'
                set_default_aerosol(lrt)
                for uo in [240,300,340,380,400,440]:
                    lrt.o3 = uo
                    lrt.mol_modify_O3 = str(uo) + ' DU'
                    lrt.run()

#%%nuvem de gelo com alteração de o3                    
lrt.outputValues = ['sza','phi','uzen','abs_param','o3','aod','ssa','gg','nstr','albedo','edn','eup','edir','radiance','reflectance']
lrt.rte_solver = 'disort'
lrt.output_process = 'per_nm'
lrt.cloudFile = 'aer.dat'
absparam = ['lowtran','reptran fine']
cldparam = ['fu']# fazer para: fu yang baum key baum_v36 hey
lrt.typeCloud = 'ice'
lrt.topCld = 9
lrt.baseCld = 8
lrt.reff = 32
lrt.iwc = 0.25
for wl in [640, 3750, 3900, 550]:
    lrt.wavelength = wl
    for wcprop in cldparam:
        lrt.ic_properties = wcprop
        for param in absparam:
            lrt.mol_abs_param = param
            lrt.abs_param = absparam.index(param)
            for atms in ['afglt']:
                lrt.textFile = 'txt_aer/gelo_o3_aer_' + str(wl) + '_'+ param +'_'+ atms +'_'+ wcprop + '_lrt.txt'
                set_default_aerosol(lrt)
                for uo in [240,300,340,380,400,440]:
                    lrt.o3 = uo
                    lrt.mol_modify_O3 = str(uo) + ' DU'
                    lrt.run()
