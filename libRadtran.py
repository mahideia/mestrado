# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 10:35:55 2016

@author: diego (e um pouco marina, né - principalmente onde nao tá comentado)
"""

#######
"""
aqui vão ficar todas as funções necessárias para rodar e ler o sbdart.
"""
#####
#%% imports necessários
import numpy as np
import functions as f
import subprocess 
from shutil import copyfile
import cStringIO
import sys

#%% variáveis super relevantes que podem ser alteradas por fora caso necessário
pathLibRadtran = None #'/home/marina/util/libRadtran-2.0/cirrus/'

class LibRadtran():
    
    #atmosfera
    atmosphere_file = None
    #espectro solar ou térmico (se tem solar não tem térmico se tem térmico não tem solar)
    source = None # (recebe solar file ou thermal file - com file opcional e colocar o per_nm quando conveniente!)    
    source_solar = None # TODO source_solar mesmo? Ou só source? pode apagar
    source_thermal = None #pode apagar
    
    #thermal grid
    thermal_grid = None    
    
    #info molecular
    mol_abs_param = None    
    mol_modify_H2O = None
    mol_modify_O3 = None
    mixing_ratio_CO2 = None
    mixing_ratio_CO = None
    mixing_ratio_CH4 = None
    mol_modify = None
    
    #aerosol
    aerosol_default = False
    aerosol_modify_tau_set = None
    aerosol_modify_ssa_set = None
    aerosol_modify_gg_set = None
    
    albedo = None
    wavelength = None
    wavelength_sup = None
    wavelength_inf = None
    rte_solver = None
    filter_function_file = None
    
    number_of_streams = None
    output_process = None
    sza = None
    latitude = None
    longitude = None
    zout = None
    phi = None
    umu = None # Caso não seja passado, usa os uzens convertendo de degrees para cos do ângulo, crescente
    
    #propriedades de nuvens    
    ic_file = None # Caso não seja definido, se tiver typeCloud define com base no 
    ic_modify_tau_set = None # Caso não seja definido, se tiver typeCloud define com base no cod
    wc_file = None # Caso não seja definido, se tiver typeCloud define com base no cod
    wc_modify_tau_set = None # Caso não seja definido, se tiver typeCloud define com base no cod
    wc_properties = None# Caso typecloud seja water, define com base no mie
    ic_properties = None
    ic_habit = None
    cod = None
    reff = None
    typeCloud = None # Determine o cálculo para tcloud - previstos water, ice, flat_water
    baseCld = None # Usado para o cálculo do usrcld.dat e determina zcloud
    topCld = None # Usado para o cálculo do usrcld.dat e determina zcloud
    interpret_as_level = False
    lwc = None #se existir, não vai usar cod pra calcular!
    iwc = None
    cloudFile = 'cloud.dat'
    cloudcover = None
    tau = None
    
    # Facilitadores/opcionais
    uzen = None # Usado para calcular umu
    output_user = None
    disort_intcor = None

    # Arquivos auxiliares
    solarFile = None 
    filterFile = None 
    mieFile = None
    filterFunction = False # Influencia isat e iout

    # Destinos do processamento
    outputFile = None
    textFile = None
    insertDatabase = False
    outputValues = None

    # Variáveis internas
    alreadyAdded = [] # Usado para impedir que em uma execução, dois valores diferentes sejam passados para o mesmo parâmetro
    lastSolarFile = None
    lastFilterFile = None
    lastMieFile = None
    lastTextFile = None

    # Método principal, executa o que foi solicitado
    def run(self):

        # Precisa ter um path configurado
        if pathLibRadtran is None:
            raise Exception('Variável pathLibRadtran precisa ser definida!')
        
        # Prepara a execução (arquivos de input, etc)
        input = self.prepare()
        print input
        sys.stdout.flush()
        

        # Executa o sbdart, pegando o resultado
        process = subprocess.Popen([pathLibRadtran + "/bin/uvspec", ""], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=None, cwd=pathLibRadtran + "/data")
        process.stdin.write(input)
        process.stdin.flush()

        outputs = process.communicate()
        #print outputs
        output = outputs[0]
        
        # Grava o arquivo de output, se um foi determinado
        if self.outputFile is not None:
            outputFile = open(self.outputFile, "w")
            outputFile.write(output)
            outputFile.flush()
            outputFile.close()
        
        # Se tem textFile ou insertDatabase, processa o arquivo de output
        if self.textFile is not None or self.insertDatabase:
            self.processOutput(output)
    
    def prepare(self):

        inputParams = ''
        self.alreadyAdded = []
        
        #se passar cod é porque não é para chamar a opção wc_modify tau set e só usar pra calcular lwc/iwc
        #se passar wc_modify ou ic_modify daí chama isso no input

        
        
        if self.typeCloud is not None:
            #monta nuvem
            if self.lwc is not None:
                lwc = self.lwc
            else:
                lwc = ((4/3)*self.cod * self.reff / f.Q_ext(self.wavelength_inf,self.reff,self.typeCloud) )/(((self.topCld - self.baseCld)*1000)/2)
                
            cldFile = open(pathLibRadtran + '/' + self.cloudFile, 'w')
            for i in [10.0,9.5,9.0,8.5,8.0,7.5,7.0,6.5,6.0,5.5,5.0,4.5,4.0,3.5,3.0,2.5,2.0,1.5,1.0]:
                if self.baseCld<= i<= self.topCld:
                    cldFile.write('{0}    {1}     {2} \n'.format(i,round(lwc, 4), self.reff))
                else:
                    cldFile.write('{0}    {1}     {2} \n'.format(i,0.0,0.0))
            cldFile.close()
            
            #pega o cod informado e determina tau
            self.tau = self.cod #/ round(np.cos(np.radians(self.sza)),4)

            

        # Arquivo filter.dat
        # Não copia de volta se o último enviado for o mesmo que o atual
        # TODO: Código que existia aqui, que não foi portado, para descobrir qual FF usar:
        # lrt.write('cp ' + pathWork + 'docs/libradtran/FF_' + str(int(param['wavelength'])) + '.dat ' + pathlibRadtran + 'filter.dat'+ '\n
        
        
        ### por enquanto não to usando isso 
        #if self.filterFile is not None and self.lastFilterFile != self.filterFile:
        #   copyfile(self.filterFile, pathLibRadtran + '/filter.dat')
        #    self.lastFilterFile = self.filterFile
        #    inputParams += self.addInputParam('filter_function_file', '../filter.dat');

        # Arquivo miefile001.mie
        # Não copia de volta se o último enviado for o mesmo que o atual
        # TODO: Código que existia aqui, que não foi portado, para descobrir qual mie file usar:
        # lrt.write('cp ' + pathWork + 'docs/libradtran/mie_' + str(int(param['wavelength'])) + '.mie ' + pathlibRadtran + 'miefile001.mie'+ '\n')
        #if self.mieFile is not None and self.lastMieFile != self.mieFile:
        #    copyfile(self.mieFile, pathLibRadtran + '/miefile001.cdf')
        #    self.lastMieFile = self.mieFile
        #inputParams += self.addInputParam('wc_properties', self.mieFile);
                

        
        if self.thermal_grid is not None:
            inputParams += self.addInputParam('wavelength_grid_file ../examples/UVSPEC_LOWTRAN_THERMAL.TRANS', ' ');
        inputParams += self.addInputParam('atmosphere_file', self.atmosphere_file);
        inputParams += self.addInputParam('source', self.source);
        inputParams += self.addInputParam('albedo', self.albedo);
        if self.wavelength is not None:
            inputParams += self.addInputParam('wavelength', '{0} {1}'.format(self.wavelength, self.wavelength));
        else:
            inputParams += self.addInputParam('wavelength','{0} {1}'.format(self.wavelength_inf, self.wavelength_sup));
        inputParams += self.addInputParam('rte_solver', self.rte_solver);
        inputParams += self.addInputParam('mol_abs_param', self.mol_abs_param);
        inputParams += self.addInputParam('mol_modify H2O', self.mol_modify_H2O);
        inputParams += self.addInputParam('mol_modify O3', self.mol_modify_O3);
        inputParams += self.addInputParam('mixing_ratio CO2', self.mixing_ratio_CO2);
        inputParams += self.addInputParam('mixing_ratio CO', self.mixing_ratio_CO);
        inputParams += self.addInputParam('mixing_ratio CH4', self.mixing_ratio_CH4);
        if self.aerosol_default: 
            inputParams += self.addInputParam('aerosol_default',' ')
        inputParams += self.addInputParam('aerosol_modify tau set', self.aerosol_modify_tau_set);   
        inputParams += self.addInputParam('aerosol_modify ssa set', self.aerosol_modify_ssa_set);  
        inputParams += self.addInputParam('aerosol_modify gg set', self.aerosol_modify_gg_set);  
        inputParams += self.addInputParam('number_of_streams', self.number_of_streams);
        inputParams += self.addInputParam('output_process', self.output_process);
        inputParams += self.addInputParam('sza', self.sza);
        inputParams += self.addInputParam('latitude', self.latitude);
        inputParams += self.addInputParam('longitude', self.longitude);
        inputParams += self.addInputParam('zout', self.zout);
        inputParams += self.addInputParam('filter_function_file', self.filter_function_file);
        inputParams += self.addInputParam('phi', self.phi);
        inputParams += self.addInputParam('umu', self.umu);
        inputParams += self.addInputParam('ic_file', self.ic_file);
        inputParams += self.addInputParam('wc_file ', self.wc_file);
        inputParams += self.addInputParam('wc_properties', self.wc_properties);
        inputParams += self.addInputParam('ic_properties', self.ic_properties);
        inputParams += self.addInputParam('ic_habit', self.ic_habit);
        inputParams += self.addInputParam('disort_intcor', self.disort_intcor);
        inputParams += self.addInputParam('output_user', self.output_user);
        
        #inputParams += self.addInputParam('mol_modify', self.mol_modify);  
        if self.interpret_as_level: inputParams += self.addInputParam('interpret_as_level', self.interpret_as_level);  

        # umu com base em uzen
        umu = []
        if self.uzen:
            for uzen in self.uzen:
                umu.append(float(format(np.round(np.cos(np.radians(uzen)), 4),'.4f')))
            umu.reverse()
            inputParams += self.addInputParam('umu', umu)
        
        # ic_file, ic_modify, wc_file e wc_modify com base no typeCloud e cod
        if self.typeCloud == 'ice':
            inputParams += self.addInputParam('cloudcover', 'ic 1.0'); 
            inputParams += self.addInputParam('ic_file', '1D ../' + self.cloudFile);  
            if self.cod is not None:
                inputParams += self.addInputParam('ic_modify', 'tau set {0}'.format(self.tau));  
        if self.typeCloud == 'water':
            inputParams += self.addInputParam('cloudcover', 'wc 1.0'); 
            inputParams += self.addInputParam('wc_file', '1D ../' + self.cloudFile);  
            if self.cod is not None:    
                inputParams += self.addInputParam('wc_modify', 'tau set {0}'.format(self.tau));  

       # if self.typeCloud == 'water':
       #     if self.mie == 'no':
       #         inputParams += self.addInputParam('interpret_as_level', 'wc');
        
        inputParams += '\nquiet\n'
        print inputParams
        return inputParams

    
    # Adiciona parâmetro ao arquivo de INPUT
    def addInputParam(self, paramName, param):
        ret = ''
        if param is not None and paramName not in self.alreadyAdded:
            if self.alreadyAdded:
                ret = '\n'
            ret += paramName + ' ' + str(param).strip("[]").replace(',','')
            
            self.alreadyAdded.append(paramName)
        return ret

    # Processa o resultado obtido do sbdart, pode jgoar num text, ou em um banco de dados
    def processOutput(self, output):

        # Cria um outputFile. É um File, mas na verdade é uma string em memória, que é bem mais rápido
        # Pode ter alguma outra forma de fazer isto, mas esta tem o menor impacto no código restante
        outputFile = cStringIO.StringIO(output)
        

        # Variável que irá armazenar os valores processados
        resultado = {}

        # Alimenta resultado com os valores de entrada, para poder utilizar eles
        # O que o método faz é jogar no dict todas as variáveis do objeto sbdart (self)
        for v in vars(self):
            resultado[v] = vars(self)[v]
        
        if self.phi != None:
            if self.output_user == None:
                # Lê as irradiâncias    
                irrad = self.readLRTIrradiance(outputFile)
                
                resultado['edir'] = irrad[0]
                resultado['edn'] = irrad[1]
                resultado['eup'] = irrad[2]
                resultado['uavgdir'] = irrad[3]
                resultado['uavgdn'] = irrad[4]
                resultado['uavgup'] = irrad[5]
                
                # Lê output de radiância: devolve um array com dimensões 
                rads = self.readLRTRadiance(outputFile)
        
                for i in range(len(self.uzen)):
                    for j in range(len(self.phi)):
                        resultado['uzen'] = self.uzen[i]
                        resultado['phi'] = self.phi[j]
                        resultado['radiance'] = rads[i,j]  
                        resultado['reflectance'] = f.reflectance(resultado['radiance'], resultado['edir'], self.sza) 
                        self.processResultLine(resultado)
            else:
                #tudo vai vir numa linha só e o uu vai vir uu(umu(0),phi(0)) ... uu(umu(0),phi(m)) ... uu(umu(n),phi(0)) ...uu(umu(n),phi(m))
                #por enquanto e como eu tenho mai o que fazer da mina vida no momento to esperando:
                #wl edir edn eup eglo uu
                #le a primeira e unica linha
                saida = self.readLRTIrradiance(outputFile)
                resultado['edir'] = saida[0]
                resultado['edn'] = saida[1]
                resultado['eup'] = saida[2]
                resultado['eglo'] = saida[3]
                
                uz = self.uzen
                uz.reverse()
                count = 3
                for u in uz:
                    for phi in self.phi:
                        count = count +1
                        resultado['uzen'] = u
                        resultado['phi'] = phi
                        resultado['radiance'] = saida[count]
                        resultado['reflectance']=f.reflectance(resultado['radiance'], resultado['edir'], self.sza) 
                        self.processResultLine(resultado)
        else: #só vai ler uma lista de wavelength + irradiancias
            #vê quantas linhas tem o arquivo a ser lido
            outputFile.seek(0)
             #le linha a linha a saída e salva no arquivo de saída
            for linha in outputFile:
                linhalida = np.fromstring(linha, sep='  ')
                resultado['wavelength']=linhalida[0]
                resultado['edir']=linhalida[1]
                resultado['edn']=linhalida[2]
                resultado['eup']=linhalida[3]
                resultado['uavgdir']=linhalida[4]
                resultado['uavgdn']=linhalida[5]
                resultado['uavgup']=linhalida[6]
                self.processResultLine(resultado)
        
    def processResultLine(self, resultado):
        
        # Se está definido para gerar arquivo text
        if self.textFile is not None:

            # Abre arquivo onde será gravado
            if self.lastTextFile is None or self.lastTextFile.name != self.textFile:
                self.lastTextFile = open(self.textFile, "a");
                self.lastTextFile.write(' '.join(self.outputValues) + '\n')

            self.lastTextFile.write(' '.join(str(resultado[key]).strip("[],'\"") for key in self.outputValues) + '\n')
            self.lastTextFile.flush()
    
#########################################
# Funções para ler as linhas e resultados do output gerado

    def readLRTIrradiance(self, arquivo):
        #lẽ primeira linha, onde tem dados de irrad 
        #estrutura = ['TOPDN','TOPUP','TOPDIR','BOTDN','BOTUP','BOTDIR']
        arquivo.seek(0)
        dados = np.transpose(self.leLinha(arquivo,1))
        lido = dados[1:]
        return lido
        
    def readLRTRadiance(self, arquivo):
         #lê linhas com dados de radiancia. 
        arquivo.seek(0)
        rads = np.loadtxt(arquivo,skiprows=2) #REVER ESQUEMA LINHA/COLUNA
        rads = np.transpose(np.transpose(rads)[2:])
        return rads

    def leLinha(self, arquivo, linha):
        arquivo.seek(0)
        for i in range(linha): 
            l = arquivo.readline()
        return np.fromstring(l, sep='  ')
        
    def file_len(self, fname):
        fname.seek(0)
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1

########
# função para montar e gravar o arquivo da nuvem  - não tá sendo usado
    def make_cloud(self):
        alturas = [10.0,9.5,9.0,8.5,8.0,7.5,7.0,6.5,6.0,5.5,5.0,4.5,4.0,3.5,3.0,2.5,2.0,1.5,1.0]
        nuvem = []
        if self.typeCloud=='water':
            for i in alturas:
                if i >= self.baseCld and i<self.topCld:
                    if self.lwc is not None:
                        lwc = self.lwc
                        nuvem.append([i,0,lwc]) #ver isso pra gelo
                    else:
                        if self.wavelength_inf is None: #não vai passar aqui
                            lwc =  self.cod*self.reff/f.Q_ext(self.wavelength,self.reff,self.typeCloud)*4/(3*(self.topCld - self.baseCld)*2000)
                        else:#nesse caso é pra intervalo, pega comprimento de onda inferior
                            lwc = ((4/3)*self.cod * self.reff / f.Q_ext(self.wavelength_inf,self.reff,'water') )/((self.topCld - self.baseCld)*2000)
                else:
                    nuvem.append([i,0,0])
        if self.typeCloud=='ice':
            for i in alturas:
                if i >= self.baseCld and i<self.topCld:
                    if self.iwc is not None:
                        iwc = self.iwc
                        nuvem.append([i,0,iwc])
                    else:
                        if self.wavelength_inf is None:
                            iwc =  self.cod*0.917*self.reff/f.Q_ext(self.wavelength,self.reff,self.typeCloud)*4/(3*(self.topCld - self.baseCld)*1000)
                        else:#nesse caso é pra intervalo, pega comprimento de onda inferior
                            iwc =  self.cod*0.917*self.reff*4/(3*f.Q_ext(self.wavelength_inf,self.reff,self.typeCloud)*(self.topCld - self.baseCld)*1000)
                        nuvem.append([i,self.cod,iwc])
                else:
                    nuvem.append([i,0,0])
        nuvem.reverse()
        return nuvem
        
            
            