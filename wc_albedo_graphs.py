# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 09:33:18 2017

@author: marina
"""

#%% imports
import numpy as np
import functions as f
import matplotlib.pyplot as pl
import graphs

caminho_leitura = '/home/marina/Dropbox/mestrado/estudos_sim/wc_albedo/ch1/dados/'
graphs.pathGraph = '/home/marina/Dropbox/mestrado/estudos_sim/wc_albedo/ch1/figuras/'
gr = graphs.Graphs()

#%%
def set_default(param):
    param['sza']=10
    param['phi']=150
    param['uzen']=10
    
    
#%% lê os dados
reff5 = np.loadtxt(caminho_leitura + 'reff5.txt')
reff10 = np.loadtxt(caminho_leitura + 'reff10.txt')
reff15 = np.loadtxt(caminho_leitura + 'reff15.txt')
reff20 = np.loadtxt(caminho_leitura + 'reff20.txt')
reff30 = np.loadtxt(caminho_leitura + 'reff30.txt')
reff40 = np.loadtxt(caminho_leitura + 'reff40.txt')

#%%
albedos = [0.125,0.1,0.075,0.05,0.025,0]
#albedos = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
cods = [0.1,1,10,25,50,75,100]
#%% gráficos
dados = reff40
wl = 'ch1'
header = ['sza','phi','uzen','albedo','cod','edir','edn','eup','eglo','radiance','reflectance']

gr.title='reff5'
gr.data_header = header
gr.header = header
condicoes = {}
set_default(condicoes)
#exclui caso default default
#dados = gr.filtra_dados(dados,condicoes,cab)
gr.data = dados
#condicoes.pop('cod')
#condicoes['albedo']=0
gr.condicoes = condicoes
#gr.ordem='y'

gr.valores_series = albedos
gr.alias_series = albedos
gr.nome_series = 'albedo'
gr.x_axis = 'cod'
gr.labelx='cod'

gr.y_axis = 'reflectance'
gr.labely='Reflectance (TOA)'
gr.figura='novo_test.png'
gr.ylim = [0.0,1.0]
gr.xlim = [0,100]
gr.plotGraph()