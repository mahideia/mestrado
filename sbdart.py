# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 10:35:55 2016

@author: diego
"""

#######
"""
aqui vão ficar todas as funções necessárias para rodar e ler o sbdart.
"""
#####
#%% imports necessários
import numpy as np
import functions as f
import subprocess 
from shutil import copyfile
import cStringIO

#%% variáveis super relevantes que podem ser alteradas por fora caso necessário
pathSBDART = None #'/home/marina/sbdart/'

class SBDART():

    #  WAVELENGTH LIMITS, FILTER FUNCTION SPECIFICATION
    nf = None
    isat = None # Caso não seja passado, se filterFunction então -1
    wlinf = None # Caso não seja passado, tenta usar wavelength / 1000
    wavelength_sup = None # Caso não seja passado, tenta usar wavelength / 1000

    # SOLAR GEOMETRY
    sza = None
    alat = None
    alon = None

    # SURFACE REFLECTANCE PROPERTIES 
    albcon = None

    # MODEL ATMOSPHERES 
    uw = None
    uo3 = None
    xco = None
    xco2 = None
    xch4 = None
    idatm = None

    # CLOUD PARAMETERS 
    zcloud = None
    tcloud = None
    nre = None # Caso não seja passado, usa reff * -1 
    lwp = None

    # BOUNDARY LAYER AEROSOLS (BLA)
    nothrm = None # Caso não seja passado, usa thermal, se False 1, se True 0

    # OUTPUT OPTIONS
    zout = None
    iout = None # Caso não seja passado, tenta usar height e zout pra determinar

    # DISORT options
    nstr = None
    uzen = None
    phi = None

    # Internos/facilitadores?
    wavelength = None # Usado para calcular wavelength_inf, wlsup e tcloud
    thermal = None # Define nothrm
    filterFunction = False # Influencia isat e iout
    typeCloud = None # Determine o cálculo para tcloud - previstos water, ice, flat_water
    baseCld = None # Usado para o cálculo do usrcld.dat e determina zcloud
    topoCld = None # Usado para o cálculo do usrcld.dat e determina zcloud
    height = None # Influencia o iout
    cod = None # Usado para o cálculo do tcloud
    cloudFile = False #define se vai ou não usar o usrcld.dat (define se nre=0 ou não)

    # É referenciado na documetação, mas não documentado. ???
    reff = None # Define nre, usado no usrcld.dat. 

    # Arquivos auxiliares
    solarFile = None 
    filterFile = None 

    # Destinos do processamento
    outputFile = None
    textFile = None
    insertDatabase = False
    outputValues = None

    # Variáveis internas
    alreadyAdded = [] # Usado para impedir que em uma execução, dois valores diferentes sejam passados para o mesmo parâmetro
    lastSolarFile = None
    lastFilterFile = None
    lastTextFile = None

    # Método principal, executa o que foi solicitado
    def run(self):

        # Precisa ter um path configurado
        if pathSBDART is None:
            raise Exception('Variável pathSBDART precisa ser definida!')
        
        # Prepara a execução (arquivos de input, etc)
        self.prepare()
        
        # Executa o sbdart, pegando o resultado
        output = subprocess.Popen([pathSBDART + "/sbdart", ""], stdout=subprocess.PIPE, cwd=pathSBDART).communicate()[0]

        # Grava o arquivo de output, se um foi determinado
        if self.outputFile is not None:
            outputFile = open(self.outputFile, "a")
            outputFile.write(output)
            outputFile.flush()
            outputFile.close()
        
        # Se tem textFile ou insertDatabase, processa o arquivo de output
        if self.textFile is not None or self.insertDatabase:
            self.processOutput(output)
    
    def prepare(self):
        # Arquivo solar.dat
        # Não copia de volta se o último enviado for o mesmo que o atual
        if self.solarFile is not None and self.lastSolarFile != self.solarFile:
            copyfile(self.solarFile, pathSBDART + '/solar.dat')
            self.lastSolarFile = self.solarFile
        
        # Arquivo filter.dat
        # Não copia de volta se o último enviado for o mesmo que o atual
        if self.filterFile is not None and self.lastFilterFile != self.filterFile:
            copyfile(self.filterFile, pathSBDART + '/filter.dat')
            self.lastFilterFile = self.filterFile

        # Arquivo usrcld.dat
        # Esse arquivo só vai funcionar se a nuvem for de água. 
        # não seja burra de querer enfiar uma nuvem de gelo pelo arquivo!
        if self.baseCld is not None and self.topoCld is not None:
            if self.cloudFile:
                usrCldFile = open(pathSBDART + '/usrcld.dat', 'w')
                for i in range(1, self.baseCld):
                    usrCldFile.write('0 0 0 0 0/\n')
                for i in range(self.baseCld, self.topoCld-1):
                     #lwp re fwp(lwp ice) reff(ice) cloudfraction(sempe 1) por eneuqanto fwp e rwffice estão zerados 
                    usrCldFile.write('{0} {1} 0 0 1 /\n'.format(f.lwp_sbd(self.cod,self.reff,self.wavelength),self.reff))
                usrCldFile.flush()
                usrCldFile.close()

        # Arquivo INPUT
        self.alreadyAdded = []
        inputFile = open(pathSBDART + '/INPUT', 'w')
        inputFile.write('&INPUT\n')

        # Se o parâmetro foi passado direto, usa ele
        self.addInputParam(inputFile, 'nf', self.nf)
        self.addInputParam(inputFile, 'idatm', self.idatm)
        self.addInputParam(inputFile, 'isat', self.isat)
        self.addInputParam(inputFile, 'wlinf', self.wavelength_inf)
        self.addInputParam(inputFile, 'wlsup', self.wavelength_sup)
        self.addInputParam(inputFile, 'sza', self.sza)
        self.addInputParam(inputFile, 'alat', self.alat)
        self.addInputParam(inputFile, 'alon', self.alon)
        self.addInputParam(inputFile, 'albcon', self.albcon)
        self.addInputParam(inputFile, 'uw', self.uw)
        self.addInputParam(inputFile, 'uo3', self.uo3)
        self.addInputParam(inputFile, 'xco', self.xco)
        self.addInputParam(inputFile, 'xco2', self.xco2)
        self.addInputParam(inputFile, 'xch4', self.xch4)
        self.addInputParam(inputFile, 'zcloud', self.zcloud)
        #self.addInputParam(inputFile, 'tcloud', self.tcloud)
        self.addInputParam(inputFile, 'nre', self.nre)
        self.addInputParam(inputFile, 'nothrm', self.nothrm)
        self.addInputParam(inputFile, 'zout', self.zout)
        #self.addInputParam(inputFile, 'iout', self.iout)
        self.addInputParam(inputFile, 'nstr', self.nstr)
        self.addInputParam(inputFile, 'uzen', self.uzen)
        self.addInputParam(inputFile, 'phi', self.phi)

        # wlinf e wlsup com base no wavelength
        if self.wavelength is not None:
            self.addInputParam(inputFile, 'wlinf', self.wavelength/1000.)
            self.addInputParam(inputFile, 'wlsup', self.wavelength/1000.)

        # isat com base no filterFunction
        if self.filterFunction:
            self.addInputParam(inputFile, 'isat', -1)

        # iout com base em height e zout
        if self.zout is not None and self.height is not None:
            print 'oi'
            if self.filterFunction is not None:
                print 'ok'
                if self.height == self.zout[1]:
                    self.addInputParam(inputFile, 'iout', 20) #se height = toa (zout(2))
                if self.height == self.zout[0]:
                    self.addInputParam(inputFile, 'iout', 21) #se height = superfície (zout(1))
            else:
                print 'else'
                if self.height == self.zout[1]:
                    self.addInputParam(inputFile, 'iout', 5) #se height = toa (zout(2))
                if self.height == self.zout[0]:
                    self.addInputParam(inputFile, 'iout', 6) #se height = superfície (zout(1))

        # thrm com base no thermal
        if self.thermal is not None:
            if self.thermal:
                self.addInputParam(inputFile, 'nothrm', 0)
            elif self.thermal == 'no':
                self.addInputParam(inputFile, 'nothrm', 1)

        # Se for nuvem (ice ou water)
        # nre com base no reff
        # zcloud com base em baseCld e topoCld
        # tcloud com base no typeCloud, cod e wavelength
        if self.typeCloud == 'ice' or self.typeCloud == 'water':
            if self.baseCld != -1:
                self.addInputParam(inputFile, 'zcloud', [self.baseCld, (self.topoCld * -1)])
            if self.typeCloud == 'ice':
                self.addInputParam(inputFile, 'tcloud', str(f.cod_ice(self.cod, self.wavelength,self.reff)) + ',1.')
            elif self.typeCloud == 'water':
                if self.lwp is None:
                    self.addInputParam(inputFile, 'tcloud', str(f.cod_water(self.cod, self.wavelength,self.reff))+ ',1.')
                else:
                    self.addInputParam(inputFile, 'lwp',str(self.lwp))
        elif self.typeCloud == 'flat_water':
            self.addInputParam(inputFile, 'tcloud', str(f.cod_water(self.cod, self.wavelength,self.reff))+ ',1.')
        
        # verifica se lerá usrcld.dat. 
        #se sim NRE =0 e todos os valores em TCLOUD, ZCLOUD e NRE serão ignorados
        # se não usa reff pra NRE (reff*-1 se for gelo)
        if self.cloudFile is not None:
            #if self.cloudFile:
            #    self.addInputParam(inputFile, 'nre', 0)
            #else:
            if self.typeCloud == 'ice':
                self.addInputParam(inputFile, 'nre', self.reff * -1)
            elif self.typeCloud == 'water':
                self.addInputParam(inputFile, 'nre', self.reff)
                    
                    
        # Salva o INPUT
        inputFile.write('\n/')
        inputFile.flush()
        inputFile.close()
    
    # Adiciona parâmetro ao arquivo de INPUT
    def addInputParam(self, inputFile, paramName, param):
        if param is not None and paramName not in self.alreadyAdded:
            if self.alreadyAdded:
                inputFile.write(',\n')
            inputFile.write(paramName + '=' + str(param).strip("[]"))
            self.alreadyAdded.append(paramName)

    # Processa o resultado obtido do sbdart, pode jgoar num text, ou em um banco de dados
    def processOutput(self, output):

        # Cria um outputFile. É um File, mas na verdade é uma string em memória, que é bem mais rápido
        # Pode ter alguma outra forma de fazer isto, mas esta tem o menor impacto no código restante
        outputFile = cStringIO.StringIO(output)

        # Variável que irá armazenar os valores processados
        resultado = {}

        # Alimenta resultado com os valores de entrada, para poder utilizar eles
        # O que o método faz é jogar no dict todas as variáveis do objeto sbdart (self)
        for v in vars(self):
            resultado[v] = vars(self)[v]

        # Lê as irradiâncias    
        irrad = self.readSBDIrradiance(outputFile)
        resultado['topdn'] = irrad[0]
        resultado['topup'] = irrad[1]
        resultado['topdir'] = irrad[2]
        resultado['botdn'] = irrad[3]
        resultado['botup'] = irrad[4]
        resultado['botdir'] = irrad[5]
        
        # Lê output de radiância: devolve um array com dimensões 
        rads = self.readSBDRadiance(outputFile)

        # Processa cada radiância, refletância, etc
        if len(self.phi) != 0:
            if len(self.uzen) == 1:
                for j in range(len(self.phi)):
                    resultado['uzen'] = self.uzen
                    resultado['phi'] = self.phi[j]
                    resultado['radiance'] = rads[j]
                    resultado['reflectance'] = f.reflectance(resultado['radiance'], resultado['topdn'], self.sza)                                    
                    self.processResultLine(resultado)
            else:
                for i in range(len(self.uzen)):
                    for j in range(len(self.phi)):
                        resultado['uzen'] = self.uzen[i]
                        resultado['phi'] = self.phi[j]
                        resultado['radiance'] = rads[i,j]
                        resultado['reflectance'] = f.reflectance(resultado['radiance'], resultado['topdn'], self.sza)
                        self.processResultLine(resultado)
        else:
            self.processResultLine(resultado)
        
    def processResultLine(self, resultado):
        
        # Se está definido para gerar arquivo text
        if self.textFile is not None:

            # Abre arquivo onde será gravado
            if self.lastTextFile is None or self.lastTextFile.name != self.textFile:
                self.lastTextFile = open(self.textFile, "w");
                self.lastTextFile.write(' '.join(self.outputValues) + '\n')

            self.lastTextFile.write(' '.join(str(resultado[key]).strip("[],'\"") for key in self.outputValues) + '\n')
            self.lastTextFile.flush()
    
#########################################
# Funções para ler as linhas e resultados do output gerado

    def leLinha(self, arquivo, linha):
        arquivo.seek(0)
        for i in range(linha): 
            l = arquivo.readline()
        return np.fromstring(l, sep='  ')

    def readSBDIrradiance(self, arquivo):
        #lẽ primeira linha, onde tem dados de irrad 
        #estrutura = ['TOPDN','TOPUP','TOPDIR','BOTDN','BOTUP','BOTDIR']
        arquivo.seek(0)
        if self.filterFunction is not None:
            dados = np.transpose(self.leLinha(arquivo,1))
            lido = dados[3:]
        else:
            dados = np.transpose(self.leLinha(arquivo,4))
            lido = dados[2:]
        return lido
            
    def readSBDRadiance(self, arquivo):
        arquivo.seek(0)
        #lê linhas com dados de radiancia. 
        if self.filterFunction is not None:
            rads = np.loadtxt(arquivo,skiprows=4)
        else:
            rads = np.loadtxt(arquivo,skiprows=7) #REVER ESQUEMA LINHA/COLUNA#REVER ESQUEMA LINHA/COLUNA        
        return rads

